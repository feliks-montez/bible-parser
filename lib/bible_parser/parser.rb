module BibleParser
  class Parser
    
    def self.parse(file)
      accepted_extensions = ['.txt']
      raise UnsupportedFileFormat if not accepted_extensions.include? File.extname(file)
      bible = {}
      if File.extname(file) == '.txt'
        File.readlines(file).each do |line|
          /(?<book>\d?\s*\w+)\s+(?<chapter>\d+):\s*(?<verse>\d+)\s+(?<body>.*)/ =~ line
          bible[book] ||= {}
          bible[book][chapter.to_i] ||= {}
          bible[book][chapter.to_i][verse.to_i] = body
        end
      end
      
      return bible
    end
    
  end
  
  class UnsupportedFileFormat < Exception
  end
end
