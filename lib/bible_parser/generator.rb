require 'json'
require 'active_support'

module BibleParser
  class Parser
    
    def self.parse(file)
      accepted_extensions = ['.txt']
      raise UnsupportedFileFormat if not accepted_extensions.include? File.extname(file)
      bible = {}
      if File.extname(file) == '.txt'
        File.readlines(file).each do |line|
          /(?<book>\d?\s*\w+)\s+(?<chapter>\d+):\s*(?<verse>\d+)\s+(?<body>.*)/ =~ line
          #book = self.get_full_book_name(book)
          bible[book] ||= {}
          bible[book][chapter.to_i] ||= {}
          bible[book][chapter.to_i][verse.to_i] = body
        end
      end
      
      return bible
    end
    
    def self.from_json(file)
      accepted_extensions = ['.json']
      raise UnsupportedFileFormat if not accepted_extensions.include? File.extname(file)
      bible = {}
      if File.extname(file) == '.json'
        json = File.read(file)
        data = JSON.parse(json)['books']

        data.each do |book, chapters|
          chapters.each do |chapter, verses|
            verses.each do |verse, body|
              #book = self.get_full_book_name(book)
              bible[book] ||= {}
              bible[book][chapter.to_i] ||= {}
              bible[book][chapter.to_i][verse.to_i] = body
            end
          end
        end
      end
      
      return bible
    end
    
    def self.get_full_book_name(book)
      self.simplify_book_name(book)
#      return "1 Corinthians" if ['1 c', '1 cor'].include? book
#      return "2 Corinthians" if ['2 c', '2 cor'].include? book
      return book
    end
    
    def self.simplify_book_name(book)
      book.gsub!(/[\.]/, '')
      book.downcase!
      #puts "#{book}\t simplified"
      book
    end
    
    def self.slugify(string)
      ret = string.strip

      #blow away apostrophes
      ret.gsub! /['`]/,""

      # @ --> at, and & --> and
      ret.gsub! /\s*@\s*/, " at "
      ret.gsub! /\s*&\s*/, " and "

      #replace all non alphanumeric, underscore or periods with underscore
       ret.gsub! /\s*[^A-Za-z0-9\.\-]\s*/, '_'  

       #convert double underscores to single
       ret.gsub! /_+/,"_"

       #strip off leading/trailing underscore
       ret.gsub! /\A[_\.]+|[_\.]+\z/,""

       ret
    end
    
  end
  
  class UnsupportedFileFormat < Exception
  end
  
  
  
  class Analyzer
    attr_accessor :bible, :to_reduce
    attr_accessor :key_verses, :keywords, :word_pairs, :word_triples
    
  
    def initialize(bible)
      @bible = bible
      @keywords = {}
      @word_pairs = {}
      @word_triples = {}
    end
    
    
    def find_keywords
      map_words
      reduce_words.each do |word, data|
        @keywords[word]     = data[0] if data.length == 1 # since there's only one occurrance, no need for an array
        @word_pairs[word]   = data    if data.length == 2
        @word_triples[word] = data    if data.length == 3
      end
      return true
    end
    
    
    def map_words
      words = []
      @bible.each do |book_name, book|
        book.each do |chapter_no, chapter|
          chapter.each do |verse_no, verse|
            # Perhaps leave commas in because with a question like "Paul, ...", having a pause in it could be key to identifying the correct verse
            #puts verse
            #puts "#{chapter_no}:#{verse_no}"# if verse.nil? 
            next if verse.nil?
            
            verse.gsub!(/[“”]/, '"')
            verse.gsub!(/[‘’]/, "'")
            verse.gsub!(/—/, "--")
            
            verse.split(/[\s(?:\-\-)—]/).each do |word|
              word.gsub!(/[,\.\?!\(\)\":;“”]/, '')
              
              # remove single quotes unless they're an apostrophe <=
              word.gsub!(/‘/, '')
              word.gsub!(/(?:’(?!\w))/, '')
              word.gsub!(/(?:(?<!\w)')|(?:'(?!\w))/, '')
              
              word.downcase!
              words << "#{word}!#{book_name}!#{chapter_no}!#{verse_no}"
            end
            
          end
        end
      end
      
      words.sort!
      @to_reduce = words
    end
    
    
    def reduce_words
      words = {}
      count = 0
      prev = nil
      @to_reduce.each do |line|
        word, book, chapter, verse = line.split('!')
        if prev.nil?
          prev = word
          words[word] = []
          count += 1
        elsif word == prev
          count += 1
        else
          words[word] = []
          prev = word
          count = 1
        end
        words[word] << {book: book, chapter: chapter.to_i, verse: verse.to_i}
      end
      
      return words
    end
    
    def self.count_key_verses(key_verses)
      count = 0
      key_verses.each do |b, chs|
        chs.each do |ch, vs|
          count += vs.size
        end
      end
      count
    end
    
  end



  class Generator
    attr_accessor :bible
    attr_accessor :key_verses, :keywords, :word_pairs, :word_triples
    
  
#    key_verses = {'1 C': {
#      1: [1,2,5,6],
#      2: [5,6,7]
#    }}
    def initialize(bible, key_verses: {}, keywords: {}, word_pairs: {}, word_triples: {})
      @bible = bible
      @key_verses = key_verses
      @keywords = keywords
      @word_pairs = word_pairs
      @word_triples = word_triples
    end
    
    def alphabetized_key_verses(filepath, up_through=21)
      key_verse_list = []
      @key_verses['John'].keys.each do |ch|
        @key_verses['John'][ch].each do |v|
          if (1..up_through).include? ch.to_i
            key_verse_list << @bible['John'][ch.to_i][v.to_i] + "  John #{ch}:#{v}\n"
          end
        end
      end
      key_verse_list.sort_by! { |v| v.downcase.gsub(/"/,'') }
      
      f = File.new(filepath, 'w+')
      key_verse_list.each do |verse|
        f.write(verse)
      end
      f.close
    end
    
    def html(filepath)
      @keywords.each do |word, location|
        b = location[:book]
        c = location[:chapter]
        v = location[:verse]
        
        verse = @bible[b][c][v]
        index = verse.downcase.index(/\b#{word}\b/)
        opening_tag = '<span class="keyword">'
        closing_tag = '</span>'
        verse.insert(index, opening_tag)
        verse.insert(index+opening_tag.size+word.size, closing_tag)
        @bible[b][c][v] = verse
      end
      
      @word_pairs.each do |word, locations|
#        puts word
        locations.each do |location|
          b = location[:book]
          c = location[:chapter]
          v = location[:verse]
          
          verse = @bible[b][c][v]
          #index = verse.downcase.index(/\b#{word}(?=\s|\.|,|\?|!|\(|\)|"|'|-|;|\:)/)
          index = verse.downcase.index(/\b#{word}\b/)
#          puts "\tindex: #{index}, verse: #{verse.downcase}"
          opening_tag = '<span class="word-pair">'
          closing_tag = '</span>'
          verse.insert(index, opening_tag)
          verse.insert(index+opening_tag.size+word.size, closing_tag)
          @bible[b][c][v] = verse
        end
      end
      
      @word_triples.each do |word, locations|
        locations.each do |location|
          b = location[:book]
          c = location[:chapter]
          v = location[:verse]
          
          verse = @bible[b][c][v]
          index = verse.downcase.index(/\b#{word}\b/)
          opening_tag = '<span class="word-triple">'
          closing_tag = '</span>'
          verse.insert(index, opening_tag)
          verse.insert(index+opening_tag.size+word.size, closing_tag)
          @bible[b][c][v] = verse
        end
      end
      
      html = "
<html>
<head>
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
  <style type=\"text/css\" rel=\"stylesheet\">
    body {
      font-size: 1.2em;
    }
    
    .index b {
      float: left;
      clear: both;
      padding: 0.2em;
      font-size: 1.5em;
    }
    
    .index a {
      float: left;
      padding: 0.2em;
      font-size: 1.5em;
    }
    
    .book-name {
      clear: both;
      margin-top: 4em;
      font-size: 2em;
    }
    
    .chapter-no {
      
    }
    
    .verse {
      margin-left: 2.4em;
      margin-bottom: 1em;
    }
    
    .verse-no {
      position: absolute;
      left: 1em;
      padding: 0.2em;
    }
    
    .verse-no.key-verse {
      background-color: #aaa;
      border-radius: 50%;
    }
    
    .keyword, .word-pair, .word-triple {
      font-weight: bold;
    }
    
    .keyword {
      //color: #ff6666;
    }
    
    .word-pair {
      
    }
    
    .word-triple {
      //color: #888;
    }
    
    .two-word-phrase {
      background-color: rgba(160, 160, 255, 0.3);
    }
  </style>
</head>
<body>
  <div class=\"key\">
    <p>Key:</p>
    <div><span class=\"keyword\">keyword</span> - word only occurs once in material (#{@keywords.size} words)</div>
    <!--<div><span class=\"word-pair\">word pair</span> - word only occurs twice in material (#{@word_pairs.size} words)</div>
    <div><span class=\"word-triple\">word triple</span> - word only occurs thrice in material (#{@word_triples.size} words)</div>-->
    <div class=\"verse\"><div class=\"verse-no key-verse\">11</div> - this is a key verse (#{Analyzer.count_key_verses(@key_verses)} verses). <!--<span style=\"color: red\">Warning: this list is not complete yet!</span>--></div>
  </div>
  <div class=\"index\">"
    @bible.each do |b, chapters|
      html << "<b>#{b}</b>"
      chapters.each do |c, _|
        html << "<a href=\"##{Parser.slugify(b+' '+c.to_s)}\">#{c}</a>"
      end
      html << "<br>"
    end
  
    html << "</div>"
      @bible.each do |b, chapters|
        html << "\t<h2 class=\"book-name\">#{b}</h2>\n"
        chapters.each do |c, verses|
          html << "\t<h3 class=\"chapter-no\"><a name=\"#{Parser.slugify(b+' '+c.to_s)}\">Chapter #{c}</h3>\n"
          verses.each do |v, verse|
            verse = @bible[b][c][v]
            begin
              key_verse = "key-verse" if @key_verses[b.to_s][c.to_s].include? v
            rescue
              key_verse = ""
            end
            html << "\t<div class=\"verse\"><div class=\"verse-no #{key_verse}\">#{v}</div>#{verse}</div>\n"
          end
        end
      end
      html << "</body>
</html>"
      
      file = File.new(filepath, 'w+')
      file.write(html)
      file.close
      
    end
    
  end
end

#@bible = BibleParser::Parser.from_json 'lib/philippians.json'
@bible = BibleParser::Parser.parse 'lib/john-bsb.txt'
@analyzer = BibleParser::Analyzer.new @bible
@analyzer.find_keywords
key_verses = JSON.parse(File.read('lib/john-bsb-key-verses.json'))
@generator = BibleParser::Generator.new(@bible, keywords: @analyzer.keywords, key_verses: key_verses)
#@generator = BibleParser::Generator.new(@bible, keywords: @analyzer.keywords, word_pairs: @analyzer.word_pairs, word_triples: @analyzer.word_triples, key_verses: {})
@generator.alphabetized_key_verses('lib/john-key-verses.txt', up_through=6)
@generator.html('lib/john-bsb.html')
#puts @analyzer.word_triples

