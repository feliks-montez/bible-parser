module BibleParser
  class Analyzer
    attr_accessor :bible, :to_reduce
    attr_accessor :keywords, :word_pairs, :word_triples
    
  
    def initialize(bible)
      @bible = bible
      @keywords = {}
      @word_pairs = {}
      @word_triples = {}
    end
    
    
    def find_keywords
      map_words
      reduce_words.each do |word, data|
        @keywords[word]     = data[0] if data.length == 1 # since there's only one occurrance, no need for an array
        @word_pairs[word]   = data    if data.length == 2
        @word_triples[word] = data    if data.length == 3
      end
      return true
    end
    
    
    def map_words
      words = []
      @bible.each do |book_name, book|
        book.each do |chapter_no, chapter|
          chapter.each do |verse_no, verse|
            # Perhaps leave commas in because with a question like "Paul, ...", having a pause in it could be key to identifying the correct verse
            verse.split(/[\s(:\-\-)]/).each do |word|
              puts word
              word.gsub!(/[,\.\?!\(\)\"\':;]/, '')
              word.downcase!
              words << "#{word}!#{book_name}!#{chapter_no}!#{verse_no}"
            end
            
          end
        end
      end
      
      words.sort!
      @to_reduce = words
    end
    
    
    def reduce_words
      words = {}
      count = 0
      prev = nil
      @to_reduce.each do |line|
        word, book, chapter, verse = line.split('!')
        if prev.nil?
          prev = word
          words[word] = []
          count += 1
        elsif word == prev
          count += 1
        else
          words[word] = []
          prev = word
          count = 1
        end
        words[word] << {book: book, chapter: chapter, verse: verse}
      end
      
      return words
    end
    
  end
end
