module BibleParser
  VERSION = File.read(File.join(File.dirname(__FILE__), "..", "VERSION")).chomp.freeze
end

require File.join(File.dirname(__FILE__), "bible_parser", "parser")
require File.join(File.dirname(__FILE__), "bible_parser", "analyzer")
require File.join(File.dirname(__FILE__), "bible_parser", "generator")
