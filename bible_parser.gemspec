Gem::Specification.new do |s|
  version = File.open('VERSION')
  s.name        = 'bible_parser'
  s.version     = version.read.gsub('\n','')
  s.date        = '2017-08-01'
  s.summary     = "a library for parsing, analyzing, and generating questions from the Bible to use for Bible Quizzing"
  s.description = "useful for creating a digital Bible Quizzing \"quiz book\""
  s.authors     = ["Brant Meier"]
  s.email       = 'bm1357@messiah.edu'
  s.files       = Dir["{doc,lib}/**/*", "MIT-LICENSE", "README.md", "VERSION"]
  #s.homepage    =
  #  'http://rubygems.org/gems/hrecord'
    
  s.add_dependency "httparty"
  #s.add_dependency ""
  
  s.license       = 'MIT'
  version.close
end
