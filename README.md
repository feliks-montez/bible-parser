# Bible Parser

A toolset for parsing the Bible, analyzing it, and generating a study Bible markup for Bible Quizzing.

- Obtain the latest version of the repository.
- Run ```gem build bible_parser.gemspec```
- Run ```gem install bible_parser-<version #>.gem```
- Done!
